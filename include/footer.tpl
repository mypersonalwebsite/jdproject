<div class="footer">
    <div class="block1">
        <div class="box">
            <ul>
                <li>
                    <div class="img">
                        <img src=__relative<<<"/imgs/footer-good-icon.png">>>>
                    </div>
                    <h1>品类齐全，轻松购物</h1>
                </li>
                <li>
                    <div class="img">
                        <img src=__relative<<<"/imgs/footer-more-icon.png">>>>
                    </div>
                    <h1>多仓直发，极速配送</h1>
                </li>
                <li>
                    <div class="img">
                        <img src=__relative<<<"/imgs/footer-quick-icon.png">>>>
                    </div>
                    <h1>正品行货，精致服务</h1>
                </li>
                <li>
                    <div class="img">
                        <img src=__relative<<<"/imgs/footer-safe-icon.png">>>>
                    </div>
                    <h1>天天低价，畅选无忧</h1>
                </li>
            </ul>
        </div>
    </div>
    <div class="block2">
        <ul>
            <li>
                <h2>购物指南<h2>
				<p><a href = '#'>购物流程</a></p>
				<p><a href = '#'>会员介绍</a></p>
				<p><a href = '#'>生活旅行</a></p>
				<p><a href = '#'>常见问题</a></p>
				<p><a href = '#'>大家电</a></p>
				<p><a href = '#'>联系客服</a></p>
			</li>
			<li>
				<h2>配送方式<h2>
				<p><a href = '#'>上门自提</a></p>
				<p><a href = '#'>211限时达</a></p>
				<p><a href = '#'>配送服务查询</a></p>
				<p><a href = '#'>配送收费标准</a></p>
				<p><a href = '#'>海外配送</a></p>
				<p><a href = '#'></a></p>
			</li>
			<li>
				<h2>支付方式<h2>
				<p><a href = '#'>货到付款</a></p>
				<p><a href = '#'>直线支付</a></p>
				<p><a href = '#'>分期付款</a></p>
				<p><a href = '#'>邮局汇款</a></p>
				<p><a href = '#'>公司转账</a></p>
				<p><a href = '#'></a></p>
			</li>
			<li>
				<h2>售货服务<h2>
				<p><a href = '#'>售后政策</a></p>
				<p><a href = '#'>价格保护</a></p>
				<p><a href = '#'>退款说明</a></p>
				<p><a href = '#'>返修/图换货</a></p>
				<p><a href = '#'>取消订单</a></p>
				<p><a href = '#'></a></p>
			</li>
			<li>
				<h2>特色服务<h2>
				<p><a href = '#'>夺宝岛</a></p>
				<p><a href = '#'>DIY装机</a></p>
				<p><a href = '#'>延保服务</a></p>
				<p><a href = '#'>京东E卡</a></p>
				<p><a href = '#'>京东通信</a></p>
				<p><a href = '#'>京东JD+</a></p>
			</li>
			<li>
				<h2>京东自营覆盖区县</h2>
                <p>
                    京东已向全国2661个区县提供自营配送服务，支持货到付款、POS机刷卡和售后上门服务。
                </p>
                <p>
                    <a href="#">查看详情&gt;</a>
                </p>
            </li>
        </ul>
    </div>
    <div class="block3">
        <ul>
            <li>
                <a href="#">关于我们</a><span>|</span>
            </li>
            <li>
                <a href="#">联系我们</a><span>|</span>
            </li>
            <li>
                <a href="#">联系客服</a><span>|</span>
            </li>
            <li>
                <a href="#">合作招商</a><span>|</span>
            </li>
            <li>
                <a href="#">商家帮助</a><span>|</span>
            </li>
            <li>
                <a href="#">销售中心</a><span>|</span>
            </li>
            <li>
                <a href="#">手机京东</a><span>|</span>
            </li>
            <li>
                <a href="#">友情链接</a><span>|</span>
            </li>
            <li>
                <a href="#">销售联盟</a><span>|</span>
            </li>
            <li>
                <a href="#">京东社区</a><span>|</span>
            </li>
            <li>
                <a href="#">风险监测</a><span>|</span>
            </li>
            <li>
                <a href="#">隐私政策</a><span>|</span>
            </li>
            <li>
                <a href="#">京东公益</a><span>|</span>
            </li>
            <li>
                <a href="#">English Site</a><span>|</span>
            </li>
            <li>
                <a href="#">Media & IR</a>
            </li>
        </ul>
    </div>
    <div class="block4">
        <p>
            <a href="#">京公网安备 11000002000088号</a><span class = 'des'>	|</span>
            <span>京ICP证070359号</span><span class = 'des'>|</span>
            <a href="#">互联网药品信息服务资格证编号(京)-经营性-2014-0008</a><span class = 'des'>	|</span>
            <span>新出发京零 字第大120007号</span>
        </p>
        <p>
            <span>互联网出版许可证编号新出网证(京)字150号</span><span class = 'des'>|</span>
            <a href="#">出版物经营许可证|网络文化经营许可证京网文[2014]2148-348号</a><span class = 'des'>|</span>
            <span>违法和不良信息举报电话：4006561155</span>
        </p>
        <p>
            <span>Copyright © 2004 - 2017  京东JD.com 版权所有</span><span class = 'des'>|</span>
            <span>消费者维权热线：4006067733</span>
            <a href="#">经营证照</a>
        </p>
        <p>
        <a href="#">京东旗下网站：京东支付</a>
        <span class = 'des'>|</span>
        <a href="#">京东云</a>
        </p>
    </div>
    <div class="block5">
        <ul>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-1.png">>>>
				</a>
            </li>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-2.png">>>>
				</a>
            </li>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-3.png">>>>
				</a>
            </li>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-4.png">>>>
				</a>
            </li>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-5.png">>>>
				</a>
            </li>
            <li>
                <a href="#">
					<img src=__relative<<<"/imgs/footer-6.png">>>>
				</a>
            </li>
        </ul>
    </div>
</div>