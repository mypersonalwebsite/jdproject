<div class="right-slide-bar">
    <div class="right-slide-bar-menu">
        <div class="menu-box">
            <ul class='menu1'>
                <li class='menu-list menu-list1' id='menu-list1' data-type='1'>
                    <a href="#">
		 				<i></i>
		 				<span>京东会员</span>
		 			</a>
                </li>
                <li class='menu-list menu-list2' id='menu-list2' data-type='2'>
                    <a href="#">
		 				<i></i>
		 				<span>购物车</span>
		 			</a>
                </li>
                <li class='menu-list menu-list3' id='menu-list3' data-type='3'>
                    <a href="#">
		 				<i></i>
		 				<span>我的关注</span>
		 			</a>
                </li>
                <li class='menu-list menu-list4' id='menu-list4' data-type='4'>
                    <a href="#">
		 				<i></i>
		 				<span>我的足迹</span>
		 			</a>
                </li>
                <li class='menu-list5' id='menu-list5'>
                    <a href="#">
		 				<i></i>
		 				<span>我的消息</span>
		 			</a>
                </li>
                <li class='menu-list menu-list6' id='menu-list6' data-type='5'>
                    <a href="#">
		 				<i></i>
		 				<span>咨询JIMI</span>
		 			</a>
                </li>
            </ul>
            <ul class='menu2'>
                <li class='menu-list menu-list7' id='menu-list7' data-type='7'>
                    <a href="#main-top">
		 				<i></i>
		 				<span>顶部</span>
		 			</a>
                </li>
                <li class='menu-list menu-list8' id='menu-list8' data-type='8'>
                    <a href="#">
		 				<i></i>
		 				<span>反馈</span>
		 			</a>
                </li>
                <li class='menu-list menu-list9' id='menu-list9' data-type='9'>
                    <a href="#">有奖调查</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="line"></div>
    <div class="content-container" id='content-container'>
        <div class="content-box content-box1" id='content-box1' style="display:none;">
            <div class="title">
                <i></i>
                <span>京东会员</span>
                <a href="#" class='remove-user-msg-btn' id='remove-user-msg-btn'></a>
            </div>
            <div class="user-msg">
                <div>
                    <div class="left-portrait">
                        <img src=__relative<<<"/imgs/nav-portrait.png">>>>
                    </div>
                    <div class="right-msg">
                        <div class='user-name'>Sunlike阿理旺旺</div>
                        <div class='level'>
                            <a href="#" class='member'>
            				<i></i>
            				<div>
            					<p>金牌会员</p>
            					<p>会员成长值：21436</p>
            					<p>还差8564可升级为钻石会员</p>
            				</div>
            			</a>
                            <a href="#" class='plus'>
            				<i></i>
            				<div>
            					会员PLUS
            				</div>
            			</a>
                        </div>
                    </div>
                </div>
                <div class="sign-in">
                    <a href="#" class='sign-in1'>会员签到领京豆</a>
                    <a href="#" class="sign-in2">金融会员签到</a>
                </div>
                <ul class='user-data'>
                    <li>
                        <a href="#">
                            <p>259</p>
                            <p>京豆</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>0</p>
                            <p>优惠券</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>0.00</p>
                            <p>钢镚</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="gif-box">
                <div class="box">
                    <i class='gif-icon'></i>
                    <span><a href="#">本月专享礼包已送到，点击领取吧</a></span>
                    <i class='remove-gif-box' id='remove-gif-box'></i>
                </div>
            </div>
            <div class="modal">
                <div class="title">
                    <span>勋章</span>
                </div>
                <ul>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>我的勋章</p>
                            <p>
                                <span>3</span>
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>专享优惠券</p>
                            <p>
                                <span>领取&gt;&gt;</span>
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>勋章活动</p>
                            <p>
                                <span>参与&gt;&gt;</span>
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="member-plus">
                <div class="title">
                    <span>会员PLUS</span>
                </div>
                <div class="description">
                    <p>恭喜你获得会员PLUS</p>
                    <p>30天免费试用资格</p>
                </div>
                <div class="sigin-in-member">
                    <a href="#">开通使用&gt;</a>
                </div>
            </div>
            <div class="finance-member">
                <div class="title">
                    <span>金融会员</span>
                </div>
                <ul>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>小白信用</p>
                            <p>
                                <span>85.8</span>
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>金币</p>
                            <p>
                                <span>114</span>
                            </p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <p>
                                <i></i>
                            </p>
                            <p>金融卡卷</p>
                            <p>
                                <span>0</span>
                            </p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>




        <div class="content-box content-box2" id='content-box2' style="display: none;">
            <div class="title">
                <a href="#">
                   <i></i>
                  <span>购物车</span>
                </a>
                <a href="#" class='remove-user-msg-btn' id='remove-user-msg-btn'></a>
            </div>
            <div class="good-container">
                <div class="good-list good-type1">
                    <div class="left-img">
                        <img src=__relative<<<"/imgs/nav-good1.png">>>>
                    </div>
                    <div class="detail">
                        <div>Lenovo/联想NGFF sl700 128G M.2 2280拯救者Y700笔记本固态</div>
                        <div>
                            <span><span>￥329.00</span><span>x1</span></span>
                            <a href="#" class='delete-good'>删除</a>
                        </div>
                    </div>
                </div>
                <div class="good-list good-type2">
                    <div class="title1">
                        <span class='tip'><span></span><span>满赠</span></span>
                        <span class='text'>已购满19元，您可领赠品</span>
                    </div>
                    <div class="content">
                        <div class="left-img">
                            <img src=__relative<<<"/imgs/nav-good2.png">>>>
                        </div>
                        <div class="detail">
                            <div>罗马仕（ROMOSS）sense6Plus 移动电源/充电宝 20000毫安 苹果/安卓/</div>
                            <div>
                                <span><span>￥109.00</span><span>x1</span></span>
                                <a href="#" class='delete-good'>删除</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="total">
            	 <div class="left">
            	 	<p><span>3</span><span></span>件商品</p>
            	 	<p><span>共计：</span><span>￥29.90</span></p>
            	 </div>
            	 <div class = 'right'>
            	 	   <a href="#" class = 'move-top-user-chart'>去购物车结算</a>
            	 </div>
            </div>
        </div>


        <div class="content-box content-box3" id='content-box3' style="display: block;">
            <div class="title">
                <a href="#">
                   <i></i>
                  <span>我的关注</span>
                </a>
                <a href="#" class='remove-user-msg-btn' id='remove-user-msg-btn'></a>
            </div>
            <div class="nav-good-menu">
                <div class="menu-list">
                    <ul>
                        <li class='good choose' id='nav-menu-good'>
                            <a href="#">商品</a>
                        </li>
                        <li class='shop' id='nav-menu-shop'>
                            <a href="#">商店</a>
                        </li>
                        <li class='activity' id='nav-menu-activity'>
                            <a href="#">活动</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="nav-menu-container">
                <div class="content-list good-content" id='nav-menu-good-content' style="display: block;">
                    <div class="content1">
                        <div class="list">
                                <div class="img">
                                    <img src=__relative<<<"/imgs/nav-good1-1.png">>>>
                                    <div class="mask">
                                        <p><a href="#" class='delete-collect-good' id='delete-collect-good'></a></p>
                                        <p><a href="#" class='add-to-chart' class='add-to-chart'>加入购物车</a></p>
                                    </div>
                                </div>
                                <div>￥4899.00</div>
                        </div>
                        <div class="list">
                                <div class="img">
                                    <img src=__relative<<<"/imgs/nav-good1-2.png">>>>
                                    <div class="mask">
                                        <p><a href="#" class='delete-collect-good' id='delete-collect-good'></a></p>
                                        <p><a href="#" class='add-to-chart' class='add-to-chart'>加入购物车</a></p>
                                    </div>
                                </div>
                                <div>暂无报价</div>
                        </div>
                        <div class="list">
                                <div class="img">
                                    <img src=__relative<<<"/imgs/nav-good1-3.png">>>>
                                    <div class="mask">
                                        <p><a href="#" class='delete-collect-good' id='delete-collect-good'></a></p>
                                        <p><a href="#" class='add-to-chart' class='add-to-chart'>加入购物车</a></p>
                                    </div>
                                </div>
                                <div>￥3549.00</div>
                        </div>
                        <div class="list">
                                <div class="img">
                                    <img src=__relative<<<"/imgs/nav-good1-4.png">>>>
                                    <div class="mask">
                                        <p><a href="#" class='delete-collect-good' id='delete-collect-good'></a></p>
                                        <p><a href="#" class='add-to-chart' class='add-to-chart'>加入购物车</a></p>
                                    </div>
                                </div>
                                <div>暂无报价</div>
                        </div>
                        <div class="list">
                                <div class="img">
                                    <img src=__relative<<<"/imgs/nav-good1-5.png">>>>
                                    <div class="mask">
                                        <p><a href="#" class='delete-collect-good' id='delete-collect-good'></a></p>
                                        <p><a href="#" class='add-to-chart' class='add-to-chart'>加入购物车</a></p>
                                    </div>
                                </div>
                                <div>暂无报价</div>
                        </div>
                    </div>
                    <div class='more'>
                        <a href="#" class='get-more-collect-good'>查看更多关注的商品&gt;&gt;</a>
                    </div>
                    <div class="no-content">
                        <div class="img">
                            <img src=__relative<<<"/imgs/cat-img.png">>>>
                        </div>
                        <div>还没有关注的商品哦~</div>
                        <div>关注后，当店铺有促销、上新时会提醒您哦~</div>
                    </div>
                </div>
                <div class="content-list shop-content" id='nav-menu-shop-content' style="display: none;">
                    <div class="content1">
                        <div class="list">
                            <div class="img">
                                <img src=__relative<<<"/imgs/nav-shop1-1.png">>>>
                            </div>
                            <div class="txt">
                                <p>Smorss自营旗舰店</p>
                                <p><a href="#">进入店铺</a></p>
                            </div>
                        </div>
                        <div class="list">
                            <div class="img">
                                <img src=__relative<<<"/imgs/nav-shop1-2.png">>>>
                            </div>
                            <div class="txt">
                                <p>Smorss自营旗舰店</p>
                                <p><a href="#">进入店铺</a></p>
                            </div>
                        </div>
                    </div>
                    <div class='more'>
                        <a href="#" class='get-more-collect-good'>更多关注的店铺&gt;&gt;</a>
                    </div>
                    <div class="no-content">
                        <div class="img">
                            <img src=__relative<<<"/imgs/cat-img.png">>>>
                        </div>
                        <div>还没有关注的店铺哦~</div>
                        <div>关注后，当店铺有促销、上新时会提醒您哦~</div>
                    </div>
                </div>
                <div class="content-list activity-content" id='nav-menu-activity-content' style="display: none;">
                    <div class="content1"></div>
                    <div class='more'>
                        <a href="#" class='get-more-collect-good'>更多关注的活动&gt;&gt;</a>
                    </div>
                    <div class="no-content">
                        <div class="img">
                            <img src=__relative<<<"/imgs/cat-img.png">>>>
                        </div>
                        <div>还没有关注的活动哦~</div>
                        <div>关注后，当有活动时会提醒您哦~</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-box content-box4" id='content-box4' style="display: none;">
            <div class="title">
                <a href="#">
                   <i></i>
                  <span>我的足迹</span>
                </a>
                <a href="#" class='remove-user-msg-btn' id='remove-user-msg-btn'></a>
            </div>
        </div>
        <div class="content-box content-box5" id='content-box5' style="display: none;">
            <div class="title">
                <a href="#">
                   <i></i>
                  <span>咨询JIMI</span>
                </a>
                <a href="#" class='remove-user-msg-btn' id='remove-user-msg-btn'></a>
            </div>
        </div>
    </div>
</div>