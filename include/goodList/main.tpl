<div class="search-container">
    <div class="search">
        <div class="direction">
            <a href="#" class='all'>全部结果:</a>
            <span>&gt;</span>
            <a href="#" class='detail'>
		  	 	<span class="name">品牌：</span>
		  	 	<span class = 'val'>Apple</span>
		  	 	<i></i>
		  	 </a>
            <a href="#" class='detail'>
		  	 	<span class="name">系统：</span>
		  	 	<span class = 'val'>（IOS）</span>
		  	 	<i></i>
		  	 </a>
            <span>&gt;</span>
            <a href="#" class='finally'>
		  	 	<span>"苹果6S plus"</span>
		  	 </a>
        </div>
        <div class="menu">
            <div class="list">
                <div class="left">分类：</div>
                <div class="right">
                    <a href="#">手机</a>
                    <a href="#">合约机</a>
                </div>
            </div>
            <div class="list">
                <div class="left">前置摄像头像素：</div>
                <div class="right">
                    <a href="#">1600万及以上</a>
                    <a href="#">800万-1599万</a>
                    <a href="#">500万-799万</a>
                    <a href="#">200万-499万</a>
                    <a href="#">120万-199万</a>
                    <a href="#">其他</a>
                    <a href="#" class='choose-more'>
		  	    		     <i>+</i>
		  	    			<span>多选</span>
		  	    		</a>
                </div>
            </div>
            <div class="list">
                <div class="left">后置摄像头像素：</div>
                <div class="right">
                    <a href="#">后置双摄像头</a>
                    <a href="#">1200万-1999万</a>
                    <a href="#">500万-1199万</a>
                    <a href="#">其他</a>
                    <a href="#" class='choose-more right-more'>
		  	    		     <i>+</i>
		  	    			<span>多选</span>
		  	    		</a>
                </div>
            </div>
            <div class="list">
                <div class="left">热点：</div>
                <div class="right">
                    <a href="#">以旧换新</a>
                    <a href="#">快速充电</a>
                    <a href="#">指纹识别</a>
                    <a href="#">VoLTE</a>
                    <a href="#">金属机身</a>
                    <a href="#">拍照神器</a>
                    <a href="#">支持NFC</a>
                    <a href="#">女性手机</a>
                    <a href="#">游戏手机</a>
                    <a href="#">其他</a>
                    <span class='right-more'>
		  	    		   <a href="#" class = 'get-more'>
		  	    		   	  <span>更多</span>
                    <i></i>
                    </a>
                    <a href="#" class='choose-more'>
		  	    		     <i>+</i>
		  	    			<span>多选</span>
		  	    		</a>
                    </span>
                </div>
            </div>
            <div class="list">
                <div class="left">高级选项：</div>
                <div class="right">
                    <ul>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>机身内存</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                    <li>
                                        <a href="#">机身内存</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>运行内存</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                    <li>
                                        <a href="#">运行内存</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>CPU核数</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">CPU核数</a>
                                    </li>
                                    <li>
                                        <a href="#">CPU核数</a>
                                    </li>
                                    <li>
                                        <a href="#">CPU核数</a>
                                    </li>
                                    <li>
                                        <a href="#">CPU核数</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>网络</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                    <li>
                                        <a href="#">网络</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>电池容量</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                    <li>
                                        <a href="#">电池容量</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>屏幕尺寸</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                    <li>
                                        <a href="#">屏幕尺寸</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class='level'>
                            <div class="show">
                                <a href="#">
		  	    						<span>机身颜色</span>
		  	    						<i></i>
		  	    					</a>
                            </div>
                            <div class="hide">
                                <ul>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                    <li>
                                        <a href="#">机身颜色</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="main">
    <div class="main-left">
        <div class="ad">
            <div class="title">
                <h2>精选品</h2>
                <span>广告</span>
            </div>
            <div class="list-box">
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
                <div class="ad-list">
                    <div class="img">
                        <a href="goodDetail.html">
	   	    		    	<img src=__relative<<<"/imgs/goodDetail/ad.png">>>>
	   	    		    </a>
                    </div>
                    <div class="detail">
                        <p class='price'>￥2998.00</p>
                        <p>
                            <a href="goodDetail.htl">
                    		vivo X9s Plus 全网通 4GB+64GB 移动联通电信
                    	    </a>
                        </p>
                        <p>
                            <span>已有</span>
                            <span class='num'>1150</span>
                            <span>人评论</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="other-ad">
            <img src=__relative<<<"/imgs/goodDetail/ad2.png">>>>
        </div>
    </div>
    <div class="main-right">
        <div class="classify">
            <ul class='classify1'>
                <li>
                    <a href="#">综合</a>
                    <i></i>
                </li>
                <li>
                    <a href="#">销量</a>
                    <i></i>
                </li>
                <li>
                    <a href="#">评论数</a>
                    <i></i>
                </li>
                <li>
                    <a href="#">新品</a>
                    <i></i>
                </li>
                <li>
                    <a href="#">价格</a>
                    <i></i>
                </li>
                <li>
                    <div class='from'>
                        <div class="show">
                            <input type="number" name="from" class='from' placeholder="￥">
                            <span>-</span>
                            <input type="number" name="from" class='from' placeholder="￥">
                        </div>
                        <div class="hide">
                            <a href="#" class='resect'>清空</a>
                            <a href="#" class='sure'>确定</a>
                        </div>
                    </div>
                </li>
                <li class='right-msg'>
                    <span>共7件商品</span>
                    <span class="now">1</span>
                    <span class='gray'>/</span>
                    <span class='total'>1</span>
                    <a href="#" class='prev'></a>
                    <a href="#" class='next'></a>
                </li>
            </ul>
            <ul class='classify2'>
                <li>配送至</li>
                <li>
                    <div class="show">广东广州天河区</div>
                </li>
                <li>
                    <input type="checkbox" name="check1">
                    <span>京东配送</span>
                </li>
                <li>
                    <input type="checkbox" name="check2">
                    <span>货到付款</span>
                </li>
                <li>
                    <input type="checkbox" name="check3">
                    <span>仅显示有货</span>
                </li>
                <li>
                    <input type="text" name="" placeholder="在结果中搜索">
                    <a href="#" class='sure-btn'>确定</a>
                </li>
            </ul>
        </div>
        <div class="good-container">
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
            <div class="good-list">
                <div class="img">
                    <img src=__relative<<<"/imgs/goodDetail/good.png">>>>
                    <div class="mask1">
                        广东无货
                    </div>
                    <div class='mask2'>
                        <a href="#">关注</a>
                    </div>
                </div>
                <div class="prize">
                    <span>￥2499.00</span>
                </div>
                <div class="d">
                    <span class='k'>苹果</span> Apple iPhone 6 32G 公开版手机 <span class='k'>苹果</span>6手机 金色 全网通4G <span class='j'>【京东发货】</span>
                </div>
                <div class="pingjia">
                    <span>已有</span>
                    <span class='num'>6000</span>
                    <span>+评论</span>
                </div>
                <div class="shop-name">
                    <span>绿森数码手机旗舰店</span>
                    <i></i>
                </div>
                <div class="way">
                    <span class='w1'>京东自营</span>
                    <span class='w2'>免邮</span>
                    <span class='w3'>赠</span>
                </div>
            </div>
        </div>
        <div class="page">
            <div class="page-box">
                <a href="#">&lt;上一页</a>
                <a href="#">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">6</a>
                <a href="#">7</a>
                <a href="#">……</a>
                <a href="#">下一页&gt;</a>
            </div>
            <div class="message">
                <span>共</span>
                <span class='num'>100</span>
                <span>页</span>
                <span>到</span>
                <input type="number" name="" value="1">
                <span>页</span>
                <a href="#" class='go-to-page'>确定</a>
            </div>
        </div>
    </div>
</div>



<div class="better-choice">
	  <div class="title">商品精选</div>
	  <div class="good-container">
	  	    <div class="good-list">
	  	    	<div class="img">
	  	    		<a href="#">
	  	    			<img src=__relative<<<"/imgs/goodDetail/choice.png">>>>
	  	    		</a>
	  	    	</div>
	  	    	<div class="d">
	  	    		<a href="#">
	  	    			vivo X9s 全网通 4GB+64GB 移动联通电信4G手机 双卡双待 金色 标准版
	  	    		</a>
	  	    	</div>
	  	    	<div class="price">
	  	    		 ￥2698.00
	  	    	</div>
	  	    	<div class="comment">
	  	    		<span class="num">1004</span>
	  	    		<span>人评价</span>
	  	    	</div>
	  	    </div>
	  	    <div class="good-list">
	  	    	<div class="img">
	  	    		<a href="#">
	  	    			<img src=__relative<<<"/imgs/goodDetail/choice.png">>>>
	  	    		</a>
	  	    	</div>
	  	    	<div class="d">
	  	    		<a href="#">
	  	    			vivo X9s 全网通 4GB+64GB 移动联通电信4G手机 双卡双待 金色 标准版
	  	    		</a>
	  	    	</div>
	  	    	<div class="price">
	  	    		 ￥2698.00
	  	    	</div>
	  	    	<div class="comment">
	  	    		<span class="num">1004</span>
	  	    		<span>人评价</span>
	  	    	</div>
	  	    </div>
	  	    <div class="good-list">
	  	    	<div class="img">
	  	    		<a href="#">
	  	    			<img src=__relative<<<"/imgs/goodDetail/choice.png">>>>
	  	    		</a>
	  	    	</div>
	  	    	<div class="d">
	  	    		<a href="#">
	  	    			vivo X9s 全网通 4GB+64GB 移动联通电信4G手机 双卡双待 金色 标准版
	  	    		</a>
	  	    	</div>
	  	    	<div class="price">
	  	    		 ￥2698.00
	  	    	</div>
	  	    	<div class="comment">
	  	    		<span class="num">1004</span>
	  	    		<span>人评价</span>
	  	    	</div>
	  	    </div>
	  	    <div class="good-list">
	  	    	<div class="img">
	  	    		<a href="#">
	  	    			<img src=__relative<<<"/imgs/goodDetail/choice.png">>>>
	  	    		</a>
	  	    	</div>
	  	    	<div class="d">
	  	    		<a href="#">
	  	    			vivo X9s 全网通 4GB+64GB 移动联通电信4G手机 双卡双待 金色 标准版
	  	    		</a>
	  	    	</div>
	  	    	<div class="price">
	  	    		 ￥2698.00
	  	    	</div>
	  	    	<div class="comment">
	  	    		<span class="num">1004</span>
	  	    		<span>人评价</span>
	  	    	</div>
	  	    </div>
	  	    <div class="good-list">
	  	    	<div class="img">
	  	    		<a href="#">
	  	    			<img src=__relative<<<"/imgs/goodDetail/choice.png">>>>
	  	    		</a>
	  	    	</div>
	  	    	<div class="d">
	  	    		<a href="#">
	  	    			vivo X9s 全网通 4GB+64GB 移动联通电信4G手机 双卡双待 金色 标准版
	  	    		</a>
	  	    	</div>
	  	    	<div class="price">
	  	    		 ￥2698.00
	  	    	</div>
	  	    	<div class="comment">
	  	    		<span class="num">1004</span>
	  	    		<span>人评价</span>
	  	    	</div>
	  	    </div>
	  </div>
</div>	

<div class="search-box">
	 <p class = 'key'>
	 	<span>您是不是要找：</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 	<span>|</span>
	 	<a href="#">苹果6s plus 128g</a>
	 </p>
	 <p class = 'word'>
	 	 <span class = 'name'>重新搜索</span>
	 	 <input type="text" name="" value="苹果6s plus">
	 	 <a href="#" class = 'search-btn'>搜索</a>
	 	 <a href="#">说说我使用搜索的感受</a>
	 </p>
</div>



<div class="my-track">
	 <div class="title">我的足迹</div>
	 <div class="box">
	 	 <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div> <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	 <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>

	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>

	 	  <div class="list">
	 	 	<div class="img">
	 	 		<img src=__relative<<<"/imgs/goodDetail/track.png">>>>
	 	 	</div>
	 	 	<div class="price">
	 	 		￥3999.00
	 	 	</div>
	 	 </div>
	 </div>
</div>