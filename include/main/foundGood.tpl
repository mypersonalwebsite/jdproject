<div class="classify1">
    <div class="found-good-thing classify1-list1">
        <div class="title">
            <h2>发现好货</h2>
            <a href="#">
                  <span>发现品质生活</span>
                  <i></i>
              </a>
        </div>
        <div class='content'>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-1.png">
                    </div>
                </div>
            </div>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-2.png">
                    </div>
                </div>
            </div>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-3.png">
                    </div>
                </div>
            </div>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-4.png">
                    </div>
                </div>
            </div>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-5.png">
                    </div>
                </div>
            </div>
            <div class="list">
                <div class="left">茵曼绣花衬衫</div>
                <div class="right">
                    <div class="img">
                        <img src="static/imgs/main/c1-1-6.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="buy-special classify1-list2">
        <div class="title">
            <h2>会买专辑</h2>
            <a href="#">
                  <span>甄选优质好物</span>
                  <i></i>
              </a>
        </div>
        <div class="content">
            <div class="left-btn1 btn1">
                <a href="#"></a>
            </div>
            <div class="right-btn1 btn1">
                <a href="#"></a>
            </div>
            <div class="mask">
                <i class='i1 choose'></i>
                <i class='i2'></i>
                <i class='i3'></i>
            </div>
            <div class="box">
                <div class="list list1">
                    <div class="line1">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                        </div>
                    </div>
                    <div class="line2">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-1.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list list2">
                    <div class="line1">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                        </div>
                    </div>
                    <div class="line2">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-2.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list list3">
                    <div class="line1">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                        </div>
                    </div>
                    <div class="line2">
                        <p>以七夕之名，送女神平板一起甜蜜刷剧</p>
                        <div class='imgs'>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                            <div class="img">
                                <img src="static/imgs/main/c2-3.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ranking-list">
        <div class="title">
            <h2>排行榜</h2>
            <a href="#">
                  <span>专属你的购物指南</span>
                  <i></i>
              </a>
        </div>
        <div class="content">
            <div class="menu">
                <div class="menu-list" data-type = '1'>
                    <a href="#">手机通信</a>
                </div>
                <div class="menu-list" data-type = '2'>
                    <a href="#">平板电脑</a>
                </div>
                <div class="menu-list" data-type = '3'>
                    <a href="#">大家电</a>
                </div>
                <div class="menu-list" data-type = '4'>
                    <a href="#">显示器</a>
                </div>
                <div class="menu-list" data-type = '5'>
                    <a href="#">手机贴膜</a>
                </div>
                <div class="bottom-border"></div>
            </div>
            <div class="menu-container">
                <div class="menu-content menu-content1">
                    <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask1">
                                    <span class='l1'>1</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask2">
                                    <span class='l2'>2</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask3">
                                    <span class='l3'>3</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask4">
                                    <span class='l4'>4</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask5">
                                    <span class='l5'>5</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-1.png">
                                <div class="mask mask6">
                                    <span class='l6'>6</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                </div>
                <div class="menu-content menu-content2">
                    <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask1">
                                    <span class='l1'>1</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask2">
                                    <span class='l2'>2</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask3">
                                    <span class='l3'>3</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask4">
                                    <span class='l4'>4</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask5">
                                    <span class='l5'>5</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-2.png">
                                <div class="mask mask6">
                                    <span class='l6'>6</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                </div>
                <div class="menu-content menu-content3">
                    <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask1">
                                    <span class='l1'>1</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask2">
                                    <span class='l2'>2</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask3">
                                    <span class='l3'>3</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask4">
                                    <span class='l4'>4</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask5">
                                    <span class='l5'>5</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-3.png">
                                <div class="mask mask6">
                                    <span class='l6'>6</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                </div>
                <div class="menu-content menu-content4">
                    <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask1">
                                    <span class='l1'>1</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask2">
                                    <span class='l2'>2</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask3">
                                    <span class='l3'>3</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask4">
                                    <span class='l4'>4</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask5">
                                    <span class='l5'>5</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-4.png">
                                <div class="mask mask6">
                                    <span class='l6'>6</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                </div>
                <div class="menu-content menu-content5">
                    <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask1">
                                    <span class='l1'>1</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask2">
                                    <span class='l2'>2</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask3">
                                    <span class='l3'>3</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask4">
                                    <span class='l4'>4</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask5">
                                    <span class='l5'>5</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                     <div class="list">
                        <a href="#">
                            <div class="img">
                                <img src="static/imgs/main/c3-5.png">
                                <div class="mask mask6">
                                    <span class='l6'>6</span>
                                </div>
                            </div>
                            <div class="detail">
                                【三片装-3D全屏】朗客 苹果6s钢化膜 苹果6钢化膜 iphone6/6s钢化膜 软边全屏覆盖 高清防爆手机保护膜(白色)
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>