<div class="slide-box">
    <div class="slide-menu">
        <ul>
            <li>
                <a href="#">秒杀</a>
            </li>
            <li>
                <a href="#">优惠券</a>
            </li>
            <li>
                <a href="#">闪购</a>
            </li>
            <li>
                <a href="#">拍卖</a>
            </li>
            <li>
                <a href="#">服装城</a>
            </li>
            <li>
                <a href="#">京东超市</a>
            </li>
            <li>
                <a href="#">生鲜</a>
            </li>
            <li>
                <a href="#">全球购</a>
            </li>
            <li>
                <a href="#">京东金融</a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div class="left-slide">
            <div class="slide" id='slide'>
                <div class="left-btn btn" id="left-btn">&lt</div>
                <div class="slide-nav">
                    <div class="slide-container" id='slide-container'></div>
                    <div class="slide-mask" id='slide-mask'></div>
                </div>
                <div class="right-btn btn" id="right-btn">&gt</div>
            </div>
            <script type="text/javascript" src='static/js/slide.js'></script>
            <div class="bottom">
              <div class="img">
                  <a href="#">
                     <img src="static/imgs/slide/b1.png">
                  </a>
              </div>
              <div class="img">
                  <a href="#">
                     <img src="static/imgs/slide/b2.png">
                  </a>
              </div>
            </div>
        </div>
        <div class="right-slide">
             
        </div>
    </div>
</div>