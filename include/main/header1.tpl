<a href="#" name = 'main-top'></a>
<div class="header1">
    <div class="head-container">
        <div class="left">
            <div class="show">
                <i class='address'></i>
                <span>送至：广东</span>
                <i class='down'></i>
            </div>
            <div class="hide">
                <ul>
                    <li><a href="#">北京</a></li>
                    <li><a href="#" class='choose'>上海</a></li>
                    <li><a href="#">天津</a></li>
                    <li><a href="#">重庆</a></li>
                    <li><a href="#">河北</a></li>
                    <li><a href="#">山西</a></li>
                    <li><a href="#">河南</a></li>
                    <li><a href="#">辽宁</a></li>
                    <li><a href="#">吉林</a></li>
                    <li><a href="#">黑龙江</a></li>
                    <li><a href="#">内蒙古</a></li>
                    <li><a href="#">江苏</a></li>
                    <li><a href="#">山东</a></li>
                    <li><a href="#">安徽</a></li>
                    <li><a href="#">浙江</a></li>
                    <li><a href="#">福建</a></li>
                    <li><a href="#">湖北</a></li>
                    <li><a href="#">湖南</a></li>
                    <li><a href="#">广东</a></li>
                    <li><a href="#">广西</a></li>
                    <li><a href="#">江西</a></li>
                    <li><a href="#">四川</a></li>
                    <li><a href="#">海南</a></li>
                    <li><a href="#">贵州</a></li>
                    <li><a href="#">云南</a></li>
                    <li><a href="#">西藏</a></li>
                    <li><a href="#">陕西</a></li>
                    <li><a href="#">甘肃</a></li>
                    <li><a href="#">青海</a></li>
                    <li><a href="#">宁夏</a></li>
                    <li><a href="#">新疆</a></li>
                    <li><a href="#">港澳</a></li>
                    <li><a href="#">台湾</a></li>
                    <li><a href="#">钓鱼岛</a></li>
                    <li><a href="#">海外</a></li>
                </ul>
            </div>
        </div>
        <div class="right">
            <ul>
                <li class='message'>
                    <div class="logouting" style="display: none">
                        <a href="login.html">请登录</a>
                        <a href="#">免费注册</a>
                    </div>
                    <div class="logining" style="display: block;">
                        <div class='show'>
                            <a href="#">
                                <span>Sunlike</span>
                                 <i class = 'plus'></i>
                                 <i class = 'down'></i>
                            </a>
                        </div>
                        <div class="hide">
                            <div class="user-message">
                                <div class="img">
                                    <img src="static/imgs/nav-portrait.png">
                                </div>
                                <div class="level">
                                    <p>
                                        <a href="#">
                                         <i></i>
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#">您可享金牌特惠，开通PLUS></a>
                                    </p>
                                </div>
                                <div class="btn">
                                    <a href="#" class='logout-btn' id='logout-btn'>退出</a>
                                </div>
                            </div>
                            <div class="privilege">
                                <div class="left-btn">
                                    <a href="#">
                                       <i></i>
                                   </a>
                                </div>
                                <div class="center">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/1.png"></p>
                                                <p>免费试用</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/2.png"></p>
                                                <p>自营运费补贴</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/3.png"></p>
                                                <p>售后服务</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/4.png"></p>
                                                <p>评价奖励</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/5.png"></p>
                                                <p>会员特价</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/6.png"></p>
                                                <p>生日礼包</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/7.png"></p>
                                                <p>专享礼包</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/8.png"></p>
                                                <p>装机服务</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/9.png"></p>
                                                <p>贵宾专线</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <p><img src="static/imgs/header/10.png"></p>
                                                <p>运费券</p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="right-btn">
                                    <a href="#">
                                       <i></i>
                                   </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class='distance'>|</li>
                <li class='order'>
                    <a href="#">我的订单</a>
                </li>
                <li class='distance'>|</li>
                <li class='jd'>
                    <div class="show">
                        <a href="#">
                            <span>我的京东</span>
                            <i></i>
                        </a>
                    </div>
                    <div class="hide">
                        <p><a href='#'>待处理订单</a><a href='#'>消息 <span class = 'red'>59</span></a></p>
                        <p><a href='#'>返修退换货</a><a href='#'>我的问答  <span class = 'red'>1303</span></a></p>
                        <p><a href='#'>降价商品</a><a href='#'>我的关注</a></p>
                        <p><a href='#'>我的京豆</a><a href='#'>我的白条</a></p>
                        <p><a href='#'>我的优惠券</a><a href='#'>我的理财</a></p>
                    </div>
                </li>
                <li class='distance'>|</li>
                <li class='member'>
                    <a href="#">京东会员</a>
                </li>
                <li class='distance'>|</li>
                <li class="company">
                    <a href="#">企业采购</a>
                </li>
                <li class='distance'>|</li>
                <li class='service'>
                    <div class='show'>
                        <a href="#">
                            <span>客户服务</span>
                            <i></i>
                        </a>
                    </div>
                    <div class="hide">
                        <p class='t'>客户</p>
                        <p class='c'><a href='#'>帮助中心</a><a href='#'>售后服务</a></p>
                        <p class='c'><a href='#'>在线客服</a><a href='#'>意见建议</a></p>
                        <p class='c'><a href='#'>电话客服</a><a href='#'>客服邮箱</a></p>
                        <p class='c'><a href='#'>金融咨询</a><a href='#'>售全球客服</a></p>
                        <p class='t'>商户</p>
                        <p class='c'><a href='#'>合作招商</a><a href='#'>京东商学院</a></p>
                        <p class='c'><a href='#'>商家后台</a><a href='#'>京麦工作台</a></p>
                        <p class='c'><a href='#'>商家帮助</a><a href='#'>规则平台</a></p>
                    </div>
                </li>
                <li class='distance'>|</li>
                <li class='nav'>
                    <div class="show">
                        <a href="#">
                           <span> 网站导航</span>
                           <i></i>
                       </a>
                    </div>
                    <div class="hide">
                        <div>
                            <p class='t'>特色主题</p>
                            <p class='c'><a href='#'>京东试用</a><a href='#'>京东金融</a><a href='#'>全球售</a></p>
                            <p class='c'><a href='#'>国际站</a><a href='#'>京东会员</a><a href='#'>京东预售</a></p>
                            <p class='c'><a href='#'>买什么</a><a href='#'>俄语站</a><a href='#'>装机大师</a></p>
                            <p class='c'><a href='#'>0元评测</a><a href='#'>定期送</a><a href='#'>港澳售</a></p>
                            <p class='c'><a href='#'>优惠券</a><a href='#'>秒杀</a><a href='#'>闪购</a></p>
                            <p class='c'><a href='#'>印尼站</a><a href='#'>企业金融服务</a><a href='#'>In货推荐</a></p>
                            <p class='c'><a href='#'>礼品购</a><a href='#'>出海招商</a></p>
                        </div>
                        <div>
                            <p class='t'>行业频道</p>
                            <p class='c'><a href='#'>手机</a><a href='#'>智能数码</a></p>
                            <p class='c'><a href='#'>玩3C</a><a href='#'>电脑办公</a></p>
                            <p class='c'><a href='#'>家用电器</a><a href='#'>京东智能</a></p>
                            <p class='c'><a href='#'>服装城</a><a href='#'>美妆馆</a></p>
                            <p class='c'><a href='#'>家装城</a><a href='#'>母婴</a></p>
                            <p class='c'><a href='#'>食品</a><a href='#'>运动户外</a></p>
                            <p class='c'><a href='#'>农资频道</a><a href='#'>整车</a></p>
                            <p class='c'><a href='#'>图书</a></p>
                        </div>
                        <div>
                            <p class='t'>生活服务</p>
                            <p class='c'><a href='#'>京东众筹</a><a href='#'>白条</a></p>
                            <p class='c'><a href='#'>京东金融App</a><a href='#'>京东小金库</a></p>
                            <p class='c'><a href='#'>理财</a><a href='#'>京东微联</a></p>
                            <p class='c'><a href='#'>话费</a><a href='#'>水电煤</a></p>
                            <p class='c'><a href='#'>彩票</a><a href='#'>旅行</a></p>
                            <p class='c'><a href='#'>机票酒店</a><a href='#'>电影票</a></p>
                            <p class='c'><a href='#'>京东到家</a><a href='#'>京东众测</a></p>
                            <p class='c'><a href='#'>游戏</a></p>
                        </div>
                        <div>
                            <p class='t'>更多精选</p>
                            <p class='c'><a href='#'>合作招商</a><a href='#'>京东通信</a></p>
                            <p class='c'><a href='#'>京东E卡</a><a href='#'>企业采购</a></p>
                            <p class='c'><a href='#'>服务市场</a><a href='#'>办公生活馆</a></p>
                            <p class='c'><a href='#'>乡村招募</a><a href='#'>校园加盟</a></p>
                            <p class='c'><a href='#'>京友邦</a><a href='#'>京东社区</a></p>
                            <p class='c'><a href='#'>智能社区</a><a href='#'>游戏社区</a></p>
                            <p class='c'><a href='#'>知识产权维权</a><a href='#'>在线读书</a></p>
                        </div>
                    </div>
                </li>
                <li class='distance'>|</li>
                <li class='phone'>
                    <div class="show">
                        <div class='text'>京东手机</div>
                        <div class="img">
                            <img src="static/imgs/show.png">
                        </div>
                    </div>
                    <div class="hide">
                        <div class="list list1">
                            <div class="img">
                                <img src="static/imgs/header/head-red-code-icon0.png">
                            </div>
                            <div class="right1">
                                <p><a href="#">京东手机</a></p>
                                <p>新人专享大礼包</p>
                                <p>
                                    <a href="#"><img src="static/imgs/header/head-iphone-icon.png"></a>
                                    <a href="#"><img src="static/imgs/header/head-android-icon.png"></a>
                                    <a href="#"><img src="static/imgs/header/head-ipad-icon.png"></a>
                                </p>
                            </div>
                        </div>
                        <div class="list list2">
                            <div class="img">
                                <img src="static/imgs/header/head-jd-code-icon.png">
                            </div>
                            <div class="right1">
                                <p>关注京东微信</p>
                                <p>微信扫一扫关注 新粉最高180元 惊喜礼包
                                </p>
                            </div>
                        </div>
                        <div class="list list3">
                            <div class="img">
                                <img src="static/imgs/header/head-jd-code-icon1.png">
                            </div>
                            <div class="right1">
                                <p><a href="#">京东金融客户端</a></p>
                                <p>新人专享大礼包</p>
                                <p>
                                    <a href="#"><img src="static/imgs/header/head-iphone-icon.png"></a>
                                    <a href="#"><img src="static/imgs/header/head-android-icon.png"></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="head-clear"></div>