<div class="get-coupon">
    <div class="container">
        <div class="coupon-center">
            <div class="title">
                <h2>领卷中心</h2>
                <a href="#">
                  <span>前往领卷中心</span>
                  <i></i>
              </a>
            </div>
            <div class="content clearfix">
            	<div class="list">
            		<div class="left">
            			 <p>
            			 	<span>￥</span>
            			 	<span>200</span>
            			 </p>
            			 <p>仅可购买京东自营华为部分手机商品</p>
            			 <p>消费满1000元可用</p>
            			 <p>更多好券</p>
            		</div>
            		<div class="right">
            			 <img src="static/imgs/getCoupon/get1-1.png">
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			 <p>
            			 	<span>￥</span>
            			 	<span>200</span>
            			 </p>
            			 <p>仅可购买京东自营华为部分手机商品</p>
            			 <p>消费满1000元可用</p>
            			 <p>更多好券</p>
            		</div>
            		<div class="right">
            			 <img src="static/imgs/getCoupon/get1-2.png">
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			 <p>
            			 	<span>￥</span>
            			 	<span>200</span>
            			 </p>
            			 <p>仅可购买京东自营华为部分手机商品</p>
            			 <p>消费满1000元可用</p>
            			 <p>更多好券</p>
            		</div>
            		<div class="right">
            			 <img src="static/imgs/getCoupon/get1-3.png">
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			 <p>
            			 	<span>￥</span>
            			 	<span>200</span>
            			 </p>
            			 <p>仅可购买京东自营华为部分手机商品</p>
            			 <p>消费满1000元可用</p>
            			 <p>更多好券</p>
            		</div>
            		<div class="right">
            			 <img src="static/imgs/getCoupon/get1-4.png">
            		</div>
            	</div>
            </div>
        </div>
        <div class="found-me">
            <div class="title">
                <h2>觅me</h2>
                <a href="#">
                  <span>搜索生活</span>
                  <i></i>
              </a>
            </div>
            <div class="content">
                <div class="get-left-btn"><i></i></div>
                <div class="get-right-btn"><i></i></div>
                <div class="list-icon">
                	<i class = choose></i>
                	<i></i>
                	<i></i>
                	<i></i>
                	<i></i>
                </div>
            	<div class="list">
            		<div class="left">
            			<img src="static/imgs/getCoupon/get2-1.png">
            		</div>
            		<div class="right">
            			<p>套住世界的不仅仅只有杜蕾斯，男人无法拒绝的万能套</p>
            			<p>
            				<span>套住世界的不仅仅只有杜... </span>
            				<a href="#">查看原文&gt;</a>
            			</p>
            			<p><i></i><span>11515</span><span>人看过</span></p>
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			<img src="static/imgs/getCoupon/get2-2.png">
            		</div>
            		<div class="right">
            			<p>套住世界的不仅仅只有杜蕾斯，男人无法拒绝的万能套</p>
            			<p>
            				<span>套住世界的不仅仅只有杜... </span>
            				<a href="#">查看原文&gt;</a>
            			</p>
            			<p><i></i><span>11515</span><span>人看过</span></p>
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			<img src="static/imgs/getCoupon/get2-3.png">
            		</div>
            		<div class="right">
            			<p>套住世界的不仅仅只有杜蕾斯，男人无法拒绝的万能套</p>
            			<p>
            				<span>套住世界的不仅仅只有杜... </span>
            				<a href="#">查看原文&gt;</a>
            			</p>
            			<p><i></i><span>11515</span><span>人看过</span></p>
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			<img src="static/imgs/getCoupon/get2-4.png">
            		</div>
            		<div class="right">
            			<p>套住世界的不仅仅只有杜蕾斯，男人无法拒绝的万能套</p>
            			<p>
            				<span>套住世界的不仅仅只有杜... </span>
            				<a href="#">查看原文&gt;</a>
            			</p>
            			<p><i></i><span>11515</span><span>人看过</span></p>
            		</div>
            	</div>
            	<div class="list">
            		<div class="left">
            			<img src="static/imgs/getCoupon/get2-5.png">
            		</div>
            		<div class="right">
            			<p>套住世界的不仅仅只有杜蕾斯，男人无法拒绝的万能套</p>
            			<p>
            				<span>套住世界的不仅仅只有杜... </span>
            				<a href="#">查看原文&gt;</a>
            			</p>
            			<p><i></i><span>11515</span><span>人看过</span></p>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>