<div class="register-container">
    <div class="outline-reg"></div>
    <div class="left-bar">
        <a href="registerQuestionnaire.html"></a>
    </div>
    <div class="register">
        <div class="register-left">
            <div class="account">
                <span>用户名</span>
                <input type="text" name="account" class='account-val' id='account-val' placeholder="您的账户名和登录名">
                <span class='pass'></span>
            </div>
            <div class="tip account-tip">
                <div class="tip1 input-tip account-input-tip" id='account-input-tip'>
                    <i>i</i>
                    <span>支持中文、字母、数字、“-”“_”的组合，4-20个字符</span>
                </div>
                <div class="tip1 error-tip account-error-tip" id='account-error-tip'>
                    <i></i>
                    <span>用户名不能是纯数字，请重新输入</span>
                </div>
                <div class="tip1 error-tip account-length-tip" id='account-length-tip'>
                    <i></i>
                    <span>长度只能在4-20个字符之间</span>
                </div>
                <div class="tip1 error-tip account-error-tip" id='account-error-tip'>
                    <i></i>
                    <span>格式错误，仅支持汉字、字母、数字、“-”“_”的组合</span>
                </div>
            </div>
            <div class="password">
                <span>设置密码</span>
                <input type="text" name="password" class='password-val' id='password-val' placeholder="建议至少两种字符组合">
                <span class='pass'></span>
            </div>
            <div class="tip password-tip">
                <div class="tip2 input-tip password-input-tip" id='password-input-tip'>
                    <i>i</i>
                    <span>建议使用字母、数字和符号两种及以上的组合，6-20个字符</span>
                </div>
                <div class="tip2 error-tip password-long-tip" id='password-long-tip'>
                    <i></i>
                    <span>长度只能在4-20个字符之间</span>
                </div>
                <div class="tip2 week-tip password-week-tip" id='password-week-tip'>
                    <i></i>
                    <span>有被盗风险,建议使用字母、数字和符号两种及以上组合</span>
                </div>
                <div class="tip2 middle-tip password-middle-tip" id='password-middle-tip'>
                    <i></i>
                    <span>安全强度适中，可以使用三种以上的组合来提高安全强度</span>
                </div>
                <div class="tip2 strong-tip password-strong-tip" id='password-strong-tip'>
                    <i></i>
                    <span>你的密码很安全</span>
                </div>
            </div>
            <div class="repassword">
                <span>确认密码</span>
                <input type="text" name="repassword" class='repassword-val' id='repassword-val' placeholder="请再次输入密码">
                <span class='pass'></span>
            </div>
            <div class="tip repassword-tip">
                 <div class="tip3 input-tip repassword-input-tip" id='repassword-input-tip'>
                    <i>i</i>
                    <span>请在此输入密码</span>
                </div>
                <div class="tip3 error-tip repassword-error-tip" id='repassword-error-tip'>
                    <i></i>
                    <span>确认密码不为空</span>
                </div>
                <div class="tip3 error-tip repassword-empty-tip" id='repassword-empty-tip'>
                    <i></i>
                    <span>两次输入密码不一致</span>
                </div>
            </div>
            <div class="check-email" id='check-email'>
                <a href="#" id='check-email-btn'>邮箱验证</a>
            </div>
            <div class="email" id='email-box'>
                <span>邮箱验证<i></i></span>
                <input type="text" name="email" class='email-val' id='email-val' placeholder="建议使用常用邮箱">
                <span class='pass'></span>
            </div>
            <div class="tip email-tip">
                <div class="tip4 input-tip email-input-tip" id='email-input-tip'>
                    <i>i</i>
                    <span>完成验证后，你可以用该邮箱登录和找回密码</span>
                </div>
                <div class="tip4 error-tip email-error-tip" id='email-error-tip'>
                    <i></i>
                    <span>格式错误</span>
                </div>
            </div>
            <div class="phone">
                <span>中国0086<i></i></span>
                <input type="text" name="phone" class='phone-val' id='phone-val' placeholder="建议使用常用手机">
                <span class='pass'></span>
            </div>
            <div class="tip phone-tip">
                <div class="tip5 input-tip phone-input-tip" id='phone-input-tip'>
                    <i>i</i>
                    <span>完成验证后，你可以用该手机登录和找回密码</span>
                </div>
                <div class="tip5 error-tip phone-empty-tip" id='phone-empty-tip'>
                    <i></i>
                    <span>手机号码不为空</span>
                </div>
                <div class="tip5 error-tip phone-error-tip" id='phone-error-tip'>
                    <i></i>
                    <span>格式有误</span>
                </div>
                <div class="tip5 exit-tip phone-exit-tip" id='phone-exit-tip'>
                    <i></i>
                    <span>号码已占用，找回账号？ 若继续注册将与原账号解绑</span>
                </div>
            </div>
            <div class="code">
                <span>验证码<i></i></span>
                <input type="text" name="code" class='code-val' id='code-val' placeholder="请输入验证码">
                <span class='pass'></span>
                <a href="#">
                       <img src="static/imgs/code-icon.jpg">
                     </a>
            </div>
            <div class="tip code-tip">
                <div class="tip6 input-tip code-input-tip" id='code-input-tip'>
                    <i>i</i>
                    <span>完成验证后，你可以用该手机登录和找回密码</span>
                </div>
                <div class="tip6 error-tip code-error-tip" id='code-error-tip'>
                    <i></i>
                    <span>格式有误</span>
                </div>
                <div class="tip6 empty-tip code-empty-tip" id='code-empty-tip'>
                    <i></i>
                    <span>请输入图片验证码</span>
                </div>
            </div>
            <div class="aggres">
                <label>
                    <input type="checkbox" name="aggress-check">
                    <span>阅读并同意</span>
                    <a href="#" class='protocol' id='protocol'>《京东用户注册协议》</a>
                    <a href="#" class='measure' id='measure'>《隐私政策》</a>
                </label>
            </div>
            <div class="btn">
                <a href="#" class='register-btn' id='register-btn'>立即注册</a>
            </div>
        </div>
        <div class="register-right">
            <div class="line1">
                <i></i>
                <a href="companyRegister.html">企业用户注册</a>
            </div>
            <div class="line2">
                <i></i>
                <a href="companyRegister.html">
                    <p>INTERNATIONAL</p>
                    <p>CUSTOMERSs</p>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="register-protocol" id = 'register-protocol' style="display: none;">
    <div class="protocol" id = 'protocol'>
        <div class="title">
            <span class="txt">京东用户协议</span>
            <a href="#" id = 'close-protocol'></a>
        </div>
    </div>
</div>
<div class="register-secret" id = 'register-secret' style="display: none;">
    <div class="secret" id = 'secret'>
        <div class="title">
            <span class="txt">隐私政策</span>
            <a href="#" id = 'close-secret'></a>
        </div>
    </div>
</div>