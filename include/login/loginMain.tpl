<div class="login-main">
	 <div class="main">
	 	<div class="login-form">
	 		 <div class="login-way">
	 		 	  <a href="#" class = 'check-code-btn choose' id = 'check-code-btn'>扫码登录</a>
	 		 	  <a href="#" class = 'check-account-btn' id = 'check-account-btn'>账户登录</a>
	 		 </div>
	 		 <div class="accout-box" id = "accout-box" style="display: none;">
	 		 	<div class="login-tip" >
		 		 	 <div class="box" style="display: block;">
		 		 	 	 <p class = 'account-empty'><i></i><span>请输入账号</span></p>
			 		 	 <p class = 'password-empty'><i></i><span>请输入密码</span></p>
			 		 	 <p class = 'acc-pass-empty'><i></i><span>请输入账号密码</span></p>
			 		 	 <p class = 'acc-pass-err'><i></i><span>账号或密码不正确</span></p>
		 		 	 </div>
		 		 </div>
		 		 <div class="accout">
		 		 	  <span>
		 		 	  	<i class = 'head'></i>
		 		 	  </span>
		 		 	  <input type="text" name="text" id = 'account-text' placeholder="邮箱/用户名/已验证手机">
		 		 	  <i class = 'clear'></i>
		 		 </div>
		 		  <div class="password">
		 		 	  <span>
		 		 	  	<i class = 'lock'></i>
		 		 	  </span>
		 		 	  <input type="password" name="text" id = 'password-text' placeholder="密码">
		 		 	  <i class = 'clear'></i>
		 		 </div>
		 		 <div class="forget">
		 		 	<a href="forgetPassword.html">忘记密码</a>
		 		 </div>
		 		 <div class="login-btn">
		 		 	<a href="#">登录</a>
		 		 </div>
	 		 </div>
	 		 <div class="code-box" id = 'code-box' style="display: block;">
	 		 	<div class="code-img choose">
	 		 		 <div class="img1" id = 'code-img'>
	 		 		 	<div class="img">
	 		 		 		<img src="static/imgs/show.png">
	 		 		 	</div>
	 		 		 	<div class="mask code-mask" id = 'code-mask' style="display: none;">
	 		 		 		  <p>二维码已失效</p>
	 		 		 		  <a href="#" class = 'get-new-code' id = 'get-new-code'>刷新</a>
	 		 		 	</div>
	 		 		 </div>
	 		 		 <div class="img2" id = 'phone-img'>
	 		 		 	<img src="static/imgs/login-phone.png">
	 		 		 </div>
	 		 	</div>
	 		 	<div class="line1">
	 		 		<span>打开</span>
	 		 		<a href="#">手机京东</a>
	 		 		<span>扫描二维码</span>
	 		 	</div>
	 		 	<div class="line2">
	 		 		<ul>
	 		 			<li>
	 		 				<i class = 'pan'></i>
	 		 				<span>免输入</span>
	 		 			</li>
	 		 			<li>
	 		 				<i class = 'light'></i>
	 		 				<span>更快</span>
	 		 			</li>
	 		 			<li>
	 		 				<i class = 'safe'></i>
	 		 				<span>更安全</span>
	 		 			</li>
	 		 		</ul>
	 		 	</div>
	 		 </div>
	 		 <div class="other-way">
	 		 	<div class="left">
	 		 		<a href = '#' class = 'way1'>
	 		 			<i></i>
	 		 			<span>QQ</span>
	 		 		</a>
	 		 		<a href = '#' class = 'way2'>
	 		 			<i></i>
	 		 			<span>微信</span>
	 		 		</a>
	 		 	</div>
	 		 	<div class="right">
	 		 		<a href="register.html">
	 		 			<i></i>
	 		 			<span>立即注册</span>
	 		 		</a>
	 		 	</div>
	 		 </div>
	 	</div>
	 </div>
</div>